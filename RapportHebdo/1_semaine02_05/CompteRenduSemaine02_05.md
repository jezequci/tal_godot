# Compte Rendu Semaine 1 (02/05-06/05)

## Lundi :
### **Matin**

#### *Horaire : 9h 12h*

Réunion Fabiani

Debut projet

Tuto Godot Part 1/3

### **Aprem**

#### *Horaire : 13h 17h*

Init Git

Suite Tuto Godot Part 1/3

Tuto Godot Part 2/3

Tuto Godot Part 3/3

------

## Mardi

### **Matin**

#### *Horaire : 9h 12h*

Simulation collision

pb : collision typé : création trop rapide

### **Aprem** 

#### *Horaire : 12h45 16h45*

simulation vaisseau sanguin 

pb : portail pour flot non fonctionnel

------

## Mercredi

### **Matin**

#### *Horaire : 9h 12h*

nouveau tuto 3d godot

### **Aprem** 

#### Horaire : *12h45 17h*

changement area en rigidbody pour meilleur collision

collision entre rigdbody :
- Virus/BloodCell -> création Virus
- Anticorps/Virus -> destruction Virus

multiplication -> problème 2 crée et multiplication trop grande

réseau de vennes : tentative utilisation objet portal

------

## Jeudi

### **Matin**

#### *Horaire : 9h 12h30*

Vaiseau Sanguin
- portail fin -> debut
- infection
- vrai veine

### **Aprem** 

#### *Horaire : 13h15 16h30*

Vaiseau Sanguin
- cellule bloodcell
- cellule virus
- cellule anticorps

------

## Vendredi

### **Matin**

#### *Horaire : 9h 12h*

réunion pascal

fix coordonnées portail

UltraVein -> nouvelle scène avec une veine différente

### **Aprem** 

#### *Horaire : 12h45 16h*

Tuto visual scripting

Collision anticorps : si touche un virus le guéri et le transforme en bloodcell *cured* qui ne peut plus se faire infecté