# Compte Rendu Semaine 4 et 5 (23/05-03/06)

# semaine 4
## Lundi :
### **Matin**

#### *Horaire : 9h 12h*

Réunion avec pascal ballet et nicolas 
Documentation du problème godot 4.0

### **Aprem**

#### *Horaire : 13h 17h*

Documentation du problème godot 4.0

Passage du projet BloodVessel en godot 3.5

Problème :
- Téléporteur, compliqué à implémenter en 3.5 fonction translate ne voulant pas déplacer l'objet depuis le script de portail.
- Donner proprement les agents au visualScript pour la transformation des bloodCell au Virus


------

## Mardi

### **Matin**

#### *Horaire : 9h 12h45*

Passage du projet BloodVessel en godot 3.5
Réunion avec pascal ballet et nicolas pour présenter nos travaux


### **Aprem** 

#### *Horaire : 13h15 16h15*

Re création from scratch du portail/téléporteur en godot 4.0 avec la nouvelle architecture de scène et de projets défini avec pascal ballet 

------

## Mercredi

### **Matin**

#### *Horaire : 9h 11h45*

Re création from scratch Simulation BloodVessel avec le portail recrée


### **Aprem** 

#### Horaire : *12h45 17h*

adaptation behaviour fusion de cellule en godot 3.5 

<div style="page-break-after: always"></div>

# semaine 5

## Lundi :
### **Matin**

#### *Horaire : 9h 12h*

debut de la rédaction du rapport et de la soutenance

### **Aprem**

#### *Horaire : 13h 17h*

codage simulation de la maladie drepanocytose

------

## Mardi

### **Matin**

#### *Horaire : *

abscence maladie


### **Aprem** 

#### *Horaire : 12h45 17h15*

codage simulation de la maladie drepanocytose

------

## Mercredi

### **Matin**

#### *Horaire : 9h 12h30*

codage simulation de la maladie drepanocytose

### **Aprem** 

#### Horaire : *13h30 17h20*

codage simulation de la maladie drepanocytose

Problème de fond : Dans le groupe tout nos programmes utilisent la collision la creation et la supression entre objet regroupé dans des boites (transformation qui avec une collision crée un objet et supprime un des réactif de la collision) cela rend nos boites plus compliqué a réutiliser pour d'autre projet nécessitant de recodé de nouvelles boites avec des comportement sensiblement similaire nous avons donc commencer a créer plus de boite plus simple (creation, destruction, application de force...)

------

## Jeudi

### **Matin**

#### *Horaire : 9h 13h15*

codage des boites (creation destruction GetPosition AddToGroup ApplyForce RandomInt)
### **Aprem** 

#### Horaire : *13h45 16h14*

application des nouvelles boîtes au simulation bloodVessel et Drepanocytose
(étonnament ça a très bien marché aucun bug a signaler)

------

## Vendredi

### **Matin**

#### *Horaire : 9h 13h30*

Rédaction rapport 
Réunion avec pascal ballet pour le rapport et la soutenance
### **Aprem** 

#### Horaire : *14h 16h30*

Rédaction rapport 