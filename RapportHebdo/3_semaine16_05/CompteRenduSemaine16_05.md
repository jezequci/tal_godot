# Compte Rendu Semaine 1 (16/05-20/05)

## Lundi :
### **Matin**

#### *Horaire : 9h 12h*

Réunion avec pascal Ballet => 
- passage de godot 3 a godot 4 
- création d'un plugin avec différent outil comportement objet agents etc...

### **Aprem**

#### *Horaire : 13h 17h*

Passage du projet BloodVessel en godot 4.0 

Problème :
- Godot 4.0 n'est pas stable nombreux crash du logiciel
- La simulation lag également avec beaucoup moins d'objet quand 3.0
- Nombreuse méthode précedemment utilisée changée en 4.0
- Documentation de ces changements pratiquement inexistante 

------

## Mardi

### **Matin**

#### *Horaire : 9h 12h*

Import des objets BloodCell Virus Vein UltraVein en godot 4.0

### **Aprem** 

#### *Horaire : 12h45 17h*

Re création from scratch du portail/téléporteur en godot 4.0 avec la nouvelle architecture de scène et de projets défini avec pascal ballet 

------

## Mercredi

### **Matin**

#### *Horaire : 9h 11h45*

Re création from scratch Simulation BloodVein

Problème de collision de la veine en 4.0 la bloodcell glitch dans la paroi intérieur également le portail marche "de temps en temps" pour aucune raison la cellule ne veut pas se téléporter 

### **Aprem** 

#### Horaire : *12h45 17h*

Essai de fix les problèmes évoqués du matin 

------

## Jeudi

### **Matin**

#### *Horaire : 9h 12h30*

Essai de fix des problèmes de la bloodVein

### **Aprem** 

#### *Horaire : 13h15 17h*

Essai de fix des problèmes de la bloodVein
Recherche et essai de coder un behaviour pour fusionner des cellules => certaine fois le cellule va rebondir donc il y a bien une collision seulement elle n'est pas détectée

------

## Vendredi

### **Matin**

#### *Horaire : 9h 12h*

réunion pascal abandon de la version 4.0 de godot beaucoup trop de problèmes lié au moteur physique de godot rendant certaine simulation impossible

### **Aprem** 

#### *Horaire : 12h45 17h*

Changement du projet BloodVessel en 3.5 avec la structure défini en 4.0