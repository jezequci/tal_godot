# Compte Rendu Semaine 1 (09/05-13/05)

## Lundi :

Absent maladie

------

## Mardi

Absent maladie

------

## Mercredi

Absent maladie

Essaie visual scripting en télétravail

------

## Jeudi

### **Matin**

#### *Horaire : 9h 12h30*

Code visual scripting deplacement aléatoire sur rigidbody 

### **Après midi** 

#### *Horaire : 13h30 17h30*

nouveau projet Proie chasseur
- creation zone de "chasse" 
- deplacement animaux
- creation mouton et loup héritant de animaux
- reutilisation du spawner utilisé pour les vaiseaux sanguins
- multiplication mouton
- collision loup mouton

------

## Vendredi

### **Matin**

#### *Horaire : 9h 12h*

projet proie chasseur :

- multiplication solution avec pourcentage de chance

- ajout de paramètre pour configurer par exemple le pourcentage de multiplication, age de mort d'un loup, nombre max de moutons

- peu importe la façon de multiplier un mouton, il ne créera jamais de mouton si on dépasse le nombre max de mouton.

### **Après midi** 

#### *Horaire : 12h45 16h45*

redaction rapport

projet proie chasseur :
- Essai changement de terrain pour l'enclos 
- changement barrière
- Essai nuage de pluie qui lache des gouttes d'eau qui transforme les loups en moutons