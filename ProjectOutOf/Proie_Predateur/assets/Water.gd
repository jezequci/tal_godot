extends RigidBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _integrate_forces(state):
	add_central_force(Vector3.ZERO)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Water_body_entered(body):
	if body.is_in_group("predateur"): 
		var mouton = load("res://assets/Mouton.tscn").instance()
		mouton.global_transform.origin = Vector3(body.transform.origin.x,-50,body.transform.origin.z)
		#mouton.translate(Vector3(body.transform.origin.x,-50,body.transform.origin.z))
		get_parent().add_child(mouton)
	queue_free()
