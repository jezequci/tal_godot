extends RigidBody
export (float) var speed = 100
export(int) var oneChanceOn = 10
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func _process(delta):
	var random_number = randi()% oneChanceOn
	if(random_number==0):
		var newPoop = load("res://assets/Poop.tscn").instance()
		newPoop.translate(Vector3(translation[0],translation[1]-1,translation[2]))
		add_child(newPoop)
		
func _integrate_forces(state):
	randomize()
	var x = rand_range(0,100)
	var y = 0
	var z = rand_range(0,100)
	var force = Vector3(x,y,z)
	add_central_force(force.normalized()*speed)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
