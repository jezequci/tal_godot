tool extends Spatial

export(bool) var ZoneSpawn = true setget set_type
export(Vector3) var ZoneScale = Vector3.ONE setget zone_scale
export(Resource) var ObjectToClone = load("res://assets/Mouton.tscn")
export(int) var numberToSpawn = 1
export(bool) var randomRotation = false
export(bool) var randomScale = false
export(Vector2) var scale_range = Vector2(0.5, 1.5)
export(int) var frequency = 60
export(bool) var on_shortcut = false
export(InputEventKey) var shortcut
export (int) var max_spawn = 100 # 100 Moutons + Point and Zone 

var cpt : int = -1
var Point
var Zone


func _ready():
	Point = get_child(0)
	Zone = get_child(1)
	if not Engine.editor_hint:
		Zone.hide()
		Point.hide()

func _process(delta):
	if not Engine.editor_hint:
		if frequency > 0:
			if cpt%frequency == 0:
				spawn()
		else:
			if cpt == -1:
				spawn()
		cpt += 1

func _unhandled_key_input(event):
	if (event.scancode == shortcut.scancode && on_shortcut):
		print("R")
		spawn()

func spawn():
	print("Mouton : ",get_child_count())
	print("Max spawn :",max_spawn)
	if (get_tree().get_nodes_in_group("proie").size() < max_spawn):
		for i in range(numberToSpawn):
			var node = ObjectToClone.instance()
			if randomRotation:
				node.set_rotation(Vector3(randf(), randf(), randf()))
			if randomScale:
				node.set_scale(Vector3.ONE * rand_range(scale_range.x, scale_range.y))
			if ZoneSpawn:
				node.set_translation(Vector3(rand_range(-ZoneScale.x/2, ZoneScale.x/2), rand_range(-ZoneScale.y/2, ZoneScale.y/2), rand_range(-ZoneScale.z/2, ZoneScale.z/2)))
			add_child(node)
		

func set_type(truc):
	Point = get_child(0)
	Zone = get_child(1)
	if truc:
		Point.hide()
		Zone.show()
		ZoneSpawn = true
	else:
		Point.show()
		Zone.hide()
		ZoneSpawn = false

func zone_scale(news):
	Zone = get_child(1)
	Zone.scale = news
	ZoneScale = news
