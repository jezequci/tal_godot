extends RigidBody
export (float) var speed = 10
#export (int) var frequence = 50
var cpt = 0


func _ready():
	pass # Replace with function body.
	
func _integrate_forces(state):
	randomize()
	var x = rand_range(-100,100)
	var y = 0
	var z = rand_range(-100,100)
	var force = Vector3(x,y,z)
	add_central_force(force.normalized()*speed)

#func _physics_process(delta):
#	cpt+=1
#	if(cpt%50):
#		add_force(Vector3.UP,translation)

