extends "res://scripts/Animaux.gd"

export(int) var oneChanceOn = 500

var multiplicationAge = 0

func _ready():
	pass
	#print("ui",get_parent().multiplicateur)

func _process(delta):
	if get_tree().get_nodes_in_group("proie").size() != get_parent().max_spawn:
		multiplicationAge += 1
		#if multiplicationAge > 200:
		var random_number = randi()% oneChanceOn
		if(random_number==0):
			var mouton = load("res://Assets/Mouton.tscn").instance()
			mouton.translate(translation)
			get_parent().add_child(mouton)
			multiplicationAge = 0
	pass


func _on_Mouton_body_entered(body):
	if body.is_in_group("predateur"):
		var loup = load("res://assets/Loup.tscn").instance()
		loup.translate(translation)
		queue_free()
		get_parent().add_child(loup)
