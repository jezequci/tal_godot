extends Area

var utiliser = false



func _process(delta):
	var random_number = randi()% 4
	match random_number:
		0: translate(Vector3(8,0,0)*delta)
		1: translate(Vector3(0,0,8)*delta)
		2: translate(Vector3(-8,0,0)*delta)
		3: translate(Vector3(0,0,-8)*delta)

func _on_CelluleB_area_entered(area):
	if (area.is_in_group("cellB") && utiliser == false ):
		utiliser = true
		var newCell = load("res://CelluleB.tscn").instance()
		var xr = randi()% 3
		var zr = randi()% 3
		newCell.translate(Vector3(transform.origin.x + xr,0,transform.origin.z + zr))
		get_parent().add_child(newCell)
		
	else :
		if (!area.is_in_group("cellB")):
			queue_free()
