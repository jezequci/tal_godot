extends RigidBody

export(float,10,20) var speed

var age = 0

func _process(delta):
	age+=1


func _integrate_forces(state):
	randomize()
	var x = rand_range(-10,10)
	var z = rand_range(-10,10)
	var force = Vector3(x,0,z)
	add_central_force(force.normalized()*speed)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_CellA_body_entered(body):
	print(body.name)
	var regex = RegEx.new()
	regex.compile("CellB[0-9]*")
	var result = regex.search(body.name)
	if(result):
		queue_free()
		
