extends RigidBody

export(float,10,20) var speed

var age =0;

func _process(delta):
	age+=1

func _integrate_forces(state):
	randomize()
	var x = rand_range(-10,10)
	var z = rand_range(-10,10)
	var force = Vector3(x,0,z)
	add_central_force(force.normalized()*speed)

		


func _on_CellB_body_entered(body):
	print(body.name)
	var regexA = RegEx.new()
	regexA.compile("CellA[0-9]*")
	var resultA = regexA.search(body.name)
	var regexB = RegEx.new()
	regexB.compile("CellB[0-9]*")
	var resultB = regexB.search(body.name)
	if(resultA):
		queue_free()
	else :
		if(resultB):
			if(age > 100 && body.age >100):
				var newCell = load("res://CellB.tscn").instance()
				print("newCell :")
				print(newCell.name)
				get_parent().add_child(newCell)
