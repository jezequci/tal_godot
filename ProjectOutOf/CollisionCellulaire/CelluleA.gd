extends Area


func _process(delta):
	var random_number = randi()% 4
	match random_number:
		0: translate(Vector3(8,0,0)*delta)
		1: translate(Vector3(0,0,8)*delta)
		2: translate(Vector3(-8,0,0)*delta)
		3: translate(Vector3(0,0,-8)*delta)


func _on_Area_area_entered(area):
	if(!area.is_in_group("CellA")):
		queue_free()
