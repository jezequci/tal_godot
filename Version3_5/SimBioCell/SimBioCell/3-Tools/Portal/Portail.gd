extends Area
export (NodePath) var other_portal
onready var node = get_node(other_portal)
export var alongAxis : String = "x"
export var groupOfAgent : String = "Cell"

var axis : int

# Called when the node enters the scene tree for the first time.
func _ready():
	match alongAxis:
		"x": 
			axis = 0
		"y":
			axis = 1
		"z": 
			axis = 2


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_Portal_body_entered(body):
	if(other_portal != ""):
		if(body.is_in_group(groupOfAgent)):
			
			match alongAxis:
				"x" :
					body.reset = true
					body.init_x = node.translation[0]+1
				"y" :
					body.reset = true
					body.init_y = node.translation[1]+1
				"z" :
					body.reset = true
					body.init_z = node.translation[2]+1
			body.add_central_force(Vector3.ZERO)
